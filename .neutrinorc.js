const tsCheckerPlugin = require('fork-ts-checker-webpack-plugin');
const lessLoader = require('neutrino-middleware-less-loader');
const sassLoader = require('neutrino-middleware-sass-loader');
const typescript = require('@kotify/neutrino-typescript');
const devServer = require('@neutrinojs/dev-server');
const airbnb = require('@neutrinojs/airbnb');
const react = require('@neutrinojs/react');
const path = require('path');

require('dotenv-defaults').config();

const options = {
  root: __dirname
};

const reactJS = {
  config: {
    env: ['DEV_PORT', 'REACT_APP_BASE_URL'],
    devtool: 'inline-source-map',
    style: {
      test: /\.(css|sass|scss|less)$/,
      targets: false,
      loaders: ['postcss-loader'],
    },
  }
};

const less = {
  config: {
    less: {
      javascriptEnabled: true,
    }
  }
}

const sass = {
  config: {
    sass: {}
  }
}

const ts = {
  config: {
    forkTSChecker: {
      options: {
        typescript: {
          memoryLimit: 3000
        },
      },
    },
  }
};

const hmr = {
  config: {
    port: process.env.DEV_PORT,
    contentBase: path.join(__dirname, 'src/static')
  }
};

const alias = () => (neutrino) => {
  neutrino.config.resolve.alias
    .set('antd-dist', 'antd/dist')
};

const plugins = () => (neutrino) => {
  neutrino.config
    .plugin('fork-ts-checker-webpack-plugin')
      .use(tsCheckerPlugin, [{async: false}])
};

module.exports = {
  options,
  use: [
    airbnb(),
    react(reactJS.config),
    lessLoader(less.config),
    sassLoader(sass.config),
    typescript(ts.config),
    devServer(hmr.config),
    alias(),
    plugins(),
  ]
};
