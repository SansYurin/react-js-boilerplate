import { useEffect, useRef } from 'react';
import { useLocation } from 'react-router-dom';

export const useScrollToBlock = () => {
  const myRef = useRef<null | HTMLDivElement>(null);
  const location = useLocation();

  useEffect(() => {
    if (myRef && location.hash.includes('#my-ref')) {
      myRef?.current?.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'center',
      });
    }
  }, [myRef, location.hash]);
};
