import styled from 'styled-components';

const Layout = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  padding: 0 12.5rem;
  width: 100%;
  max-width: 1440px;
`;

const Logo = styled.img.attrs(() => ({
  alt: 'logo',
}))`
  display: flex;
  flex: 0 0 auto;
  width: 18rem;
  object-fit: contain;
`;

export {
  Layout,
  Logo,
};
