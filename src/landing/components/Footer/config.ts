const config = {
  logoSrc: '/images/logo.svg',
  mailLink: 'support@cryptoservers.net',
};

export default config;
