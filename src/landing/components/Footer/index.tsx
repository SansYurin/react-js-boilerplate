import React, { FC } from 'react';
import {
  Wrapper,
  Content,
  InnerBox,
  FooterLogo,
  Text,
  MailLink,
  ArrowUp,
} from './styles';

import config from './config';

const Footer:FC = () => {
  return (
    <Wrapper>
      <Content>
        <InnerBox>
          <FooterLogo src={config.logoSrc} />
          <Text>
            Copyright 2020 © CryptoServers. All rights reserved.
          </Text>
          <MailLink
            as='a'
            href={`mailto:${config.mailLink}`}
          >
            {config.mailLink}
          </MailLink>
          <ArrowUp />
        </InnerBox>
      </Content>
    </Wrapper>
  );
};

export default Footer;
