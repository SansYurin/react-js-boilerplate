import styled from 'styled-components';
import { Layout, Logo } from '../styles';
import { colors } from '../../../styles/variables';

const Wrapper = styled.footer`
  grid-area: footer;
  background-color: ${colors.$black3};
  box-shadow: 0px -4px 10px ${colors.$black4};
  z-index: 1;
`;

const Content = styled(Layout)`
  padding-top: 2.45rem;
  padding-bottom: 2.15rem;
`;

const InnerBox = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
`;

const FooterLogo = styled(Logo)`
  margin-bottom: 8px;
`;

const Text = styled.span`
  display: inline-flex;
  font-size: 1.4rem;
  color: ${colors.$white1};
`;

const MailLink = styled(Text)`
  margin-top: 10px;
  color: ${colors.$white1};
  text-decoration: underline;
`;

const ArrowUp = styled.span`
  display: flex;
  position: absolute;
  flex: 0 0 auto;
  top: 50%;
  right: 0;
  margin-right: -5px;
  padding: 5px;
  width: 50px;
  height: 50px;
  transform: translateY(-50%);
  cursor: pointer;
  &::after {
    content: '';
    position: relative;
    width: 100%;
    height: 100%;
    background-image: url('/images/up-arrow.svg');
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
  };
`;

export {
  Wrapper,
  Content,
  InnerBox,
  FooterLogo,
  Text,
  MailLink,
  ArrowUp,
};
