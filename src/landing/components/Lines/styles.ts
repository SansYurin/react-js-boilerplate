import styled from 'styled-components';
import { colors } from '../../../styles/variables';

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  position: absolute;
  left: 50%;
  max-width: 1200px;
  width: 100%;
  height: 100%;
  transform: translateX(-50%);
  z-index: 0;
`;

const Line = styled.div`
  position: relative;
  border-right: 1px solid ${colors.$purple};
  opacity: .3;
  &:first-child {
    border-left: 1px solid ${colors.$purple};
  }
`;

export {
  Wrapper,
  Line,
};
