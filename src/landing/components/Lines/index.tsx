import React, { FC } from 'react';
import {
  Wrapper,
  Line
} from './styles';

const Lines: FC = () => {
  return (
    <Wrapper>
      <Line />
      <Line />
      <Line />
      <Line />
    </Wrapper>
  )
};

export default Lines;
