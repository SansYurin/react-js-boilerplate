import React, { FC, ReactNode } from 'react';
import { Wrapper } from './styles';

interface IProps {
  children?: ReactNode;
}

const Content: FC<IProps> = ({
  children,
}) => (
  <Wrapper as='main'>
    { children }
  </Wrapper>
);

export default Content;
