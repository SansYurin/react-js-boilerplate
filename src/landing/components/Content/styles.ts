import styled from 'styled-components';
import { Layout } from '../styles';

const Wrapper = styled(Layout)`
  grid-area: content;
  z-index: 2;
`;

export {
  Wrapper,
};
