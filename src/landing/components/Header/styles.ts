import styled from 'styled-components';
import { Layout, Logo } from '../styles';
import { colors } from '../../../styles/variables';

const btnStyles = `
  margin-top: 5px;
  padding: .6rem 1.6rem .5rem;
  background-color: ${colors.$white2};
  &:hover {
    background-color: ${colors.$white3};
  }
`;

const Wrapper = styled.header`
  grid-area: header;
  position: sticky;
  top: 0;
  z-index: 3;
`;

const Content = styled(Layout)`
  flex-direction: row;
  flex-flow: row nowrap;
  align-items: center;
  height: 100%;
`;

const HeaderLogo = styled(Logo)`
  margin-top: 5px;
`;

const LoginText = styled.span`
  display: inline-flex;
  font-weight: 300;
  line-height: 1;
  &::before {
    content: '';
    display: flex;
    flex: 0 0 auto;
    margin: 0 9px 2px 0;
    width: 1rem;
    background-image: url('/images/personal.svg');
    background-size: contain;
    background-position: center;
    background-repeat: no-repeat;
  }
`;

export {
  btnStyles,
  Wrapper,
  Content,
  HeaderLogo,
  LoginText,
};
