import React, { FC } from 'react';
import {
  Button,
} from '../../../components';
import {
  Nav,
} from './components';
import {
  btnStyles,
  Wrapper,
  Content,
  HeaderLogo,
  LoginText,
} from './styles';

import config from './config';

const Header: FC = () => {
  return (
    <Wrapper>
      <Content>
        <HeaderLogo src={config.logoSrc} />
        <Nav menu={config.menu} />
        <Button
          type='button'
          styles={btnStyles}
        >
          <LoginText>
            Личный кабинет
          </LoginText>
        </Button>
      </Content>
    </Wrapper>
  );
};

export default Header;
