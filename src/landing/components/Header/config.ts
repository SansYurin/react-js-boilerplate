const config = {
  logoSrc: '/images/logo.svg',
  menu: [
    {
      id: 'test_drive',
      name: 'Тест-драйв',
      isHash: false,
      isModalTrigger: true,
    },
    {
      id: 'location',
      name: 'Локации',
      isHash: true,
      isModalTrigger: false,
    },
    {
      id: 'prices',
      name: 'Цены',
      isHash: true,
      isModalTrigger: false,
    },
  ],
};

export default config;
