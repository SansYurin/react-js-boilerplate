export interface INavItem {
  id: string;
  name: string;
  isHash: boolean;
  isModalTrigger: boolean;
};
