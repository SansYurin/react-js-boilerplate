import styled from 'styled-components';
import { colors } from '../../../../../styles/variables';

const Wrapper = styled.nav`
  display: flex;
  align-items: center;
  margin-left: auto;
  margin-right: 60px;
  height: 100%;
`;

const List = styled.ul`
  display: flex;
  margin: 0;
  padding: 1rem 0 .5rem;
`;

const Item = styled.li`
  display: flex;
  align-items: center;
  line-height: 1;
  &:not(:last-child) {
    margin-right: 59px;
  }
`;

const AnchorLink = styled.a`
  color: ${colors.$white1};
  text-decoration: underline;
  &:hover {
    color: ${colors.$white};
    text-decoration: underline;
  }
`;

export {
  Wrapper,
  List,
  Item,
  AnchorLink,
};
