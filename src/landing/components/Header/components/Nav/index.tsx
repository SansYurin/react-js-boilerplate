import React, { FC } from 'react';
import { INavItem } from '../../types';
import {
  Wrapper,
  List,
  Item,
  AnchorLink,
} from './styles';

interface IProps {
  menu: Array<INavItem>;
}

const Nav: FC<IProps> = ({
  menu,
}) => {
  return (
    <Wrapper>
      <List>
        {
          menu.map(({ id, name }) => (
            <Item
              key={id}
            >
              <AnchorLink>
                {name}
              </AnchorLink>
            </Item>
          ))
        }
      </List>
    </Wrapper>
  );
};

export default Nav;
