import React, { FC } from 'react';
import {
  Header,
  Content,
  Footer,
  Lines,
} from '../components';

const Landing: FC = () => (
  <>
    <Lines />
    <Header />
    <Content>
      Landing Page
    </Content>
    <Footer />
  </>
);

export default Landing;
