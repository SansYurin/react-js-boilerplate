import { shallowEqual, useSelector } from 'react-redux';
import { IState } from '../store/types';

const useShallowEqualSelector = (selector: (state: IState) => any) => (
  useSelector(selector, shallowEqual)
);

export default useShallowEqualSelector;

