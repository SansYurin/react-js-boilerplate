import React, { ReactNode, FC } from 'react';
import {
  StyledButton,
} from './styles';

interface IProps {
  children?: ReactNode;
  styles?: string;
  [x:string]: any;
}

const Button: FC<IProps> = ({
  children,
  styles,
  ...props
}) => {
  return (
    <StyledButton
      {...props}
      styles={styles}
    >
      {children}
    </StyledButton>
  );
};

export default Button;
