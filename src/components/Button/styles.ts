import styled from 'styled-components';

interface IProps {
  styles?: string;
}

const StyledButton = styled.button<IProps>`
  display: inline-block;
  padding: 1rem 1.6rem;
  outline: 0;
  border: 0;
  border-radius: 2px;
  background-color: transparent;
  transition: background-color .2s ease-in-out;
  cursor: pointer;
  &:active,
  &:focus {
    outline: 0;
    border: 0;
  }

  ${({ styles }) => styles || ''};
`;

export {
  StyledButton,
};
