export interface ILoginForm {
  email: string;
  password: string;
  device_name: string;
}

export interface IRegisterForm {
  email: string;
  password: string;
  device_name: string;
}

export interface IAuthState {
  isAuth: boolean;
}
