import { handleActions } from 'redux-actions';
import { types } from './actionTypes';
import { IAuthState } from './types';

const initialState: IAuthState = {
  isAuth: false,
};

export const auth = handleActions<IAuthState, any>({
  [types.LOGIN__SUCCESS]: (state, { payload }) => ({
    ...state,
    isAuth: true,
  }),
  [types.LOGIN__FAILURE]: (state, { payload }) => ({
    ...state,
    isAuth: false,
  }),
  [types.REGISTER__SUCCESS]: (state, { payload }) => ({
    ...state,
  }),
  [types.REGISTER__FAILURE]: (state, { payload }) => ({
    ...state,
  }),
}, initialState);
