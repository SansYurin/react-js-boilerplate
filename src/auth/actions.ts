import { createAction } from 'redux-actions';
import { types } from './actionTypes';
import { ILoginForm, IRegisterForm } from './types';

export const login = createAction(
  types.LOGIN__FETCH,
  (form: ILoginForm) => ({ form }),
);

export const register = createAction(
  types.REGISTER__FETCH,
  (form: IRegisterForm) => ({ form }),
);
