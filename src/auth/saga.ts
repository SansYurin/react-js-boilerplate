import { SagaIterator } from '@redux-saga/core';
import { put, takeLatest } from 'redux-saga/effects';
import { AnyAction } from 'redux';
import { equals } from 'ramda';
import { types } from './actionTypes';

function* login({ payload: { form } }: AnyAction): SagaIterator {
  try {
    const response = { status: 200 };
    if (equals(response.status, 200)) {
      yield put({
        type: types.LOGIN__SUCCESS,
        payload: 'success',
      });
    }
  } catch (error) {
    yield put({
      type: types.LOGIN__FAILURE,
      payload: 'failure',
    });
  }
}

function* register({ payload: { form } }: AnyAction): SagaIterator {
  try {
    const response = { status: 200 };
    if (equals(response.status, 200)) {
      yield put({
        type: types.REGISTER__SUCCESS,
        payload: 'success',
      });
    }
  } catch (error) {
    yield put({
      type: types.REGISTER__FAILURE,
      payload: 'failure',
    });
  }
}

export function* auth() {
  yield takeLatest(types.LOGIN__FETCH, login);
  yield takeLatest(types.REGISTER__FETCH, register);
}
