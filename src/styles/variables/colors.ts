const colors = {
  $white: '#ffffff',
  $white1: 'rgba(255, 255, 255, 0.7)',
  $white2: 'rgba(255, 255, 255, 0.3)',
  $white3: 'rgba(255, 255, 255, 0.4)',

  $black: '#000000',
  $black1: '#141414',
  $black2: '#1D1D1D',
  $black3: 'rgba(0, 0, 0, 0.6)',
  $black4: 'rgba(0, 0, 0, 0.25)',

  $blue: '#177DDC',

  $purple: '#45108A',
  $purple1: '#3D065F',
  $purple2: '#10054D',
};

export default colors;
