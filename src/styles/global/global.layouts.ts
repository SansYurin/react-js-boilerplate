import styled from 'styled-components';

const AppLandingContainer = styled.div`
  display: grid;
  grid-template-areas:
    'header'
    'content'
    'footer';
  grid-template-columns: 1fr;
  grid-template-rows: 70px 1fr auto;
  grid-gap: 0;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const AppPersonalContainer = styled.div`
  display: grid;
  grid-template-areas:
    'sidebar header'
    'sidebar content';
  width: 100%;
  height: 100%;
`;

const LandingLayout = styled(AppLandingContainer)`
  background: ${({ theme }) => theme.landing.background};
  color: ${({ theme }) => theme.landing.color};
`;

const PersonalLayout = styled(AppPersonalContainer)`
  background: ${({ theme }) => theme.personal.background};
  color: ${({ theme }) => theme.personal.color};
`;

export {
  LandingLayout,
  PersonalLayout,
};
