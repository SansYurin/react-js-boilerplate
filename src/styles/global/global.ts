import { createGlobalStyle } from 'styled-components';
import { fonts } from '../variables';

export const GlobalStyles = createGlobalStyle`
  html{
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    -webkit-overflow-scrolling: touch;
    font-size: 62.5%;
  }
  body {
    display: flex;
    flex-direction: column;
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    min-height: 100%;
    overflow-y: auto;
    font-size: 1.4rem;
    font-family: ${fonts.$formular};
    * {
      box-sizing: border-box;
      -webkit-font-smoothing: antialiased;
    }
    #root {
      display: flex;
      flex: 1;
    }
  }
`;

export default GlobalStyles;
