import React, { FC } from 'react';
import { Button } from 'antd';

const Servers: FC = () => (
  <Button type='primary' size='large'>
    Servers
  </Button>
);

export default Servers;
