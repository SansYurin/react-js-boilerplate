import { useEffect } from 'react';
import { History } from 'history';
import {
  useHistory,
  useLocation,
  useRouteMatch,
} from 'react-router-dom';
import { equals } from 'ramda';

interface MatchParams {
  path: string;
}

interface LocationParams {
  pathname: string;
}

const DEFAULT_ROUTE_PATH = 'servers';

export const useDefaultSection = (): void => {
  const history = useHistory<History>();
  const location = useLocation<LocationParams>();
  const match = useRouteMatch<MatchParams>();

  useEffect(() => {
    if (equals(location.pathname, match.path)) {
      history.push(`${match.path}/${DEFAULT_ROUTE_PATH}`);
    }
  });
};
