import React, { FC } from 'react';
import {
  Switch,
  Route,
  Link,
} from 'react-router-dom';
import { useDefaultSection } from './hooks';

interface IProps {
  routes: any;
}

const Personal: FC<IProps> = ({ routes }) => {
  useDefaultSection();
  return (
    <div>
      <h1>Personal</h1>
      <ul>
        {routes.map((route: any, index: number) => (
          <li key={index}>
            <Link to={route.path}>
              {route.title}
            </Link>
          </li>
        ))}
        <Switch>
          {routes.map((route: any, index: number) => (
            <Route
              key={`route-${index}`}
              {...route}
            />
          ))}
        </Switch>
      </ul>
    </div>
  );
};

export default Personal;
