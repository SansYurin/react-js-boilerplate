import { hot, setConfig } from 'react-hot-loader';
import React, { FC } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import { Router } from './routing';

import './styles/less/index.less';
import './styles/sass/index.scss';

const App: FC = () => (
  <Provider store={store}>
    <Router />
  </Provider>
);

setConfig({
  showReactDomPatchNotification: false,
});

export default hot(module)(App);
