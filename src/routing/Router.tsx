import React, { FC } from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { LandingRouter, ProtectedRouter } from './routers';
import { routes, protectedRoutes } from './config';
import setTheme from '../themes';
import { IState } from '../store/types';
import { useShallowEqualSelector } from '../hooks';
import {
  GlobalStyles,
  GlobalFonts,
} from '../styles';

const Router: FC = () => {
  const { auth, theme } = useShallowEqualSelector(
    (state: IState) => ({ auth: state.auth, theme: state.theme }),
  );

  return (
    <BrowserRouter>
      <ThemeProvider theme={setTheme(theme)}>
        <GlobalStyles />
        <GlobalFonts />
        <Switch>
          {routes.map((route, index) => (
            <LandingRouter
              key={`layout-${index}`}
              path={route.path}
              exact={route.exact}
              Component={route.component}
            />
          ))}
          {protectedRoutes.map((route, index) => (
            <ProtectedRouter
              key={`privateLayout-${index}`}
              exact={route.exact}
              isAuth={auth.isAuth}
              path={route.path}
              routes={route.routes}
              Component={route.component}
            />
          ))}
        </Switch>
      </ThemeProvider>
    </BrowserRouter>
  );
};

export default Router;
