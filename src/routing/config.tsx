import { IRoute } from './types';
import { Landing } from '../landing';
import { Login, Register } from '../auth';
import * as personal from '../personal';

export const routes: Array<IRoute> = [
  {
    path: '/',
    title: 'Landing',
    description: '',
    exact: true,
    component: Landing,
  },
  {
    path: '/login',
    title: 'Login',
    description: '',
    exact: true,
    component: Login,
  },
  {
    path: '/registration',
    title: 'Registration',
    description: '',
    exact: true,
    component: Register,
  },
];

export const protectedRoutes: Array<IRoute> = [
  {
    path: '/personal',
    title: 'Personal',
    description: '',
    exact: false,
    component: personal.Main,
    routes: [
      {
        path: '/personal/servers',
        title: 'Servers',
        description: '',
        exact: true,
        component: personal.Servers,
        routes: [],
      },
      {
        path: '/personal/snapshots',
        title: 'Snapshots',
        description: '',
        exact: true,
        component: personal.Snapshots,
        routes: [],
      },
      {
        path: '/personal/ssh-keys',
        title: 'SSH keys',
        description: '',
        exact: true,
        component: personal.SSHKeys,
        routes: [],
      },
      {
        path: '/personal/api-keys',
        title: 'API keys',
        description: '',
        exact: true,
        component: personal.APIKeys,
        routes: [],
      },
      {
        path: '/personal/account',
        title: 'Balance and Accounts',
        description: '',
        exact: true,
        component: personal.Account,
        routes: [],
      },
      {
        path: '/personal/settings',
        title: 'Settings',
        description: '',
        exact: true,
        component: personal.Settings,
        routes: [],
      },
      {
        path: '/personal/chat',
        title: 'Chat',
        description: '',
        exact: true,
        component: personal.Chat,
        routes: [],
      },
      {
        path: '/personal/support',
        title: 'Support',
        description: '',
        exact: true,
        component: personal.Support,
        routes: [],
      },
    ],
  },
];
