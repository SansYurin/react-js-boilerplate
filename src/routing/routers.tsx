import React, { FC, ComponentType } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { LandingLayout, PersonalLayout } from '../styles';

interface IProps {
  isAuth?: boolean;
  Component: ComponentType<any>;
  [x: string]: any;
}

export const LandingRouter: FC<IProps> = ({
  Component,
  ...props
}) => (
  <Route
    {...props}
    render={(transferProps) => (
      <LandingLayout>
        <Component {...transferProps} />
      </LandingLayout>
    )}
  />
);

export const ProtectedRouter: FC<IProps> = ({
  isAuth,
  routes,
  Component,
  ...props
}) => (
  <Route
    {...props}
    render={(transferProps) => (isAuth
      ? (
        <PersonalLayout>
          <Component
            {...transferProps}
            routes={routes}
          />
        </PersonalLayout>
      )
      : <Redirect to='/login' />)}
  />
);
