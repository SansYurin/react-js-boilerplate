import { ComponentType } from 'react';
import { IAuthState } from '../auth/types';
import { IThemeState } from '../themes/types';

export interface IPropsRouter {
  auth: IAuthState;
  theme: IThemeState;
};

export interface IRoute {
  path: string;
  title: string;
  description: string;
  exact: boolean;
  component: ComponentType<any>;
  routes?: Array<IRoute>;
}
