import { handleActions } from 'redux-actions';
import { types } from './actionTypes';
import { IThemeState } from './types';

const initialState: IThemeState = {
  landing: 'default',
  personal: 'dark',
};

export const theme = handleActions<IThemeState, any>({
  [types.THEME__MOUNT]: (state, { payload }) => ({
    ...state,
    ...payload,
  }),
}, initialState);
