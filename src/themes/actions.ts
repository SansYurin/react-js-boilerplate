import { createAction } from 'redux-actions';
import { types } from './actionTypes';

export const setTheme = createAction(
  types.THEME_SET__FETCH,
  (theme: string) => ({ theme }),
);
