import { colors } from '../styles/variables';

export const landing = {
  default: {
    name: 'default',
    background: `linear-gradient(
      294.61deg, ${colors.$purple} 2.27%,
      ${colors.$purple1} 29.57%,
      ${colors.$purple2} 101.35%
    )`,
    color: `${colors.$white}`,
  },
};
