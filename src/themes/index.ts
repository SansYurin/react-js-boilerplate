import { landing } from './landing';
import { personal } from './personal';

const landingThemes = new Map([
  ['default', landing.default],
]);

const personalThemes = new Map([
  ['dark', personal.dark],
  ['default', personal.dark],
]);

const getTheme = (
  themeName: string,
  themStore: Map<string, object>,
) => (
  themStore.has(themeName)
    ? themStore.get(themeName)
    : themStore.get('default')
);

const setTheme = (themeOptions: {
  landing: string;
  personal: string;
}) => ({
  landing: getTheme(themeOptions.landing, landingThemes),
  personal: getTheme(themeOptions.personal, personalThemes),
});

export default setTheme;
