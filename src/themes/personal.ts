import { colors } from '../styles/variables';

export const personal = {
  dark: {
    name: 'dark',
    background: `${colors.$black}`,
    color: `${colors.$white}`,
  },
};
