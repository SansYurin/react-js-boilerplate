import { SagaIterator } from '@redux-saga/core';
import { put, takeLatest } from 'redux-saga/effects';
import { AnyAction } from 'redux';
import { equals } from 'ramda';
import { types } from './actionTypes';

function* setTheme({ payload: { form } }: AnyAction): SagaIterator {
  try {
    const response = { status: 200 };
    if (equals(response.status, 200)) {
      yield put({
        type: types.THEME__MOUNT,
        payload: {
          landing: 'default',
          personal: 'dark',
        },
      });
    }
  } catch (error) {
    yield put({
      type: types.THEME_SET__FAILURE,
      payload: 'failure',
    });
  }
}

export function* theme() {
  yield takeLatest(types.THEME_SET__FETCH, setTheme);
}
