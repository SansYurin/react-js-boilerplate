export const types = {
  THEME_SET__FETCH: '@@theme/THEME_SET__FETCH',
  THEME_SET__FAILURE: '@@theme/THEME_SET__FAILURE',
  THEME__MOUNT: '@@theme/THEME__MOUNT',
};
