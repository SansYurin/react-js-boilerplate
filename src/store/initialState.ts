import { IState } from './types';

export const initialState: IState = {
  auth: {
    isAuth: true,
  },
};
