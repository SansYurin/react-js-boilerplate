import { SagaIterator } from 'redux-saga';
import { all, call } from 'redux-saga/effects';
import { auth } from '../auth/saga';
import { theme } from '../themes/saga';

export default function* rootSaga(): SagaIterator {
  yield all([
    call(auth),
    call(theme),
  ]);
}
