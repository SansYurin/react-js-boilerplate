import { IAuthState } from '../auth/types';
import { IThemeState } from '../themes/types';

export interface IState {
  auth: IAuthState;
  theme?: IThemeState;
}
