import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import { not, equals } from 'ramda';
import { initialState as state } from './initialState';
import { IState } from './types';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

export const history = createBrowserHistory();

const development = not(equals(process.env.NODE_ENV, 'production'));
const sagaMiddleware = createSagaMiddleware();

// Function to call to configure Redux store
const configureStore = (initialState: IState) => {
  const enhancer = composeWithDevTools(
    development
    // Middleware you want to use in development
      ? applyMiddleware(createLogger(), sagaMiddleware)
    // Middleware you want to use in production
      : applyMiddleware(sagaMiddleware),
  );

  const store = createStore(
    rootReducer,
    initialState,
    enhancer,
  );

  sagaMiddleware.run(rootSaga);

  return store;
};

export default configureStore(state);
