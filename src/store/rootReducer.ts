import { combineReducers } from 'redux';
import { auth } from '../auth/reducer';
import { theme } from '../themes/reducer';

const rootReducer = combineReducers({
  auth,
  theme,
});

export default rootReducer;
